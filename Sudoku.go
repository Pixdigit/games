package main

import (
	"fmt"
	"math/rand"
	"time"
	"gitlab.com/Pixdigit/games/sudoku"
)

func init() {
	rand.Seed(time.Now().Unix())
}

func main() {
	found := 0
	tries := 1000
	allIter := 0
	allGuesses := 0
	falseGuesses := 0
	maxIter := 0
	longestSolveSteps := []sudoku.Field{}
	for i := 0; i < tries; i++ {
		s := sudoku.NewField()
		// s.grid = make([]int, 9*9)

		for j := 0; j < 9*9; j++ {
			s.SetAt(rand.Intn(9), rand.Intn(9), 0)
		}

		_, guesses, success, steps := sudoku.Solve(s)

		allGuesses += guesses
		if success {
			found++
			if len(steps) > maxIter {
				maxIter = len(steps)
				longestSolveSteps = steps
			}
			allIter += len(steps)
		} else {
			falseGuesses += guesses
		}
	}
	for _, solveStep := range longestSolveSteps {
		fmt.Println(solveStep)
	}

	fmt.Println("Solvable Sodokus: ", found, "(", float64(found)/float64(tries)*100, "%)")
	fmt.Println("Longest Solve   : ", maxIter)
	fmt.Println("Average Solve   : ", float64(allIter)/float64(found))
	fmt.Println("Total Guesses   : ", allGuesses)
	fmt.Println("Average Guesses : ", float64(allGuesses)/float64(found))
	fmt.Println("False Guesses   : ", falseGuesses)
}
