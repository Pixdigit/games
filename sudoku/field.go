package sudoku

import (
	"strconv"
	"strings"
)

type Field struct {
	grid []int
}

func NewField() Field {
	grid := []int{
		1, 2, 3, 4, 5, 6, 7, 8, 9,
		7, 8, 9, 1, 2, 3, 4, 5, 6,
		4, 5, 6, 7, 8, 9, 1, 2, 3,

		2, 3, 1, 5, 6, 4, 8, 9, 7,
		8, 9, 7, 2, 3, 1, 5, 6, 4,
		5, 6, 4, 8, 9, 7, 2, 3, 1,

		3, 1, 2, 6, 4, 5, 9, 7, 8,
		9, 7, 8, 3, 1, 2, 6, 4, 5,
		6, 4, 5, 9, 7, 8, 3, 1, 2,
	}

	field := Field{grid}
	return field
}

func LoadField(fieldData []int) Field {
	//TODO: check length
	f := NewField()
	f.grid = fieldData
	return f
}

func (f Field) Clone() Field {
	cloneGrid := make([]int, len(f.grid), len(f.grid))
	copy(cloneGrid, f.grid)
	cloneField := LoadField(cloneGrid)
	return cloneField
}

func (f *Field) SetAt(x, y, num int) {
	index := x + y*9

	f.grid[index] = num
}

func (f Field) GetAt(x, y int) (int, bool) {
	index := x + y*9
	num := f.grid[index]
	return num, num != 0
}

func (f *Field) Mirror(horizonzal, vertical bool) {
	originalField := f.Clone()

	for x := 0; x < 9; x++ {
		for y := 0; y < 9; y++ {
			num, _ := originalField.GetAt(x, y)
			dstX := x
			dstY := y
			if vertical {
				dstX = 9 - 1 - x
			}
			if horizonzal {
				dstY = 9 - 1 - y
			}
			f.SetAt(dstX, dstY, num)
		}
	}
}

func (f *Field) Rotate(clockwise bool) {
	if clockwise {
		//see https://www.geeksforgeeks.org/inplace-rotate-square-matrix-by-90-degrees/ for explaination
		for y := 0; y < 9/2; y++ {
			for x := y; x < 9-1-y; x++ {
				top, _ := f.GetAt(x, y)

				//right to top
				right, _ := f.GetAt(y, 9-1-x)
				f.SetAt(x, y, right)

				//bottom to right
				bottom, _ := f.GetAt(9-1-x, 9-1-y)
				f.SetAt(y, 9-1-x, bottom)

				//left to bottom
				left, _ := f.GetAt(9-1-y, x)
				f.SetAt(9-1-x, 9-1-y, left)

				//top to left
				f.SetAt(9-1-y, x, top)
			}
		}
	} else {
		f.Rotate(true)
		f.Rotate(true)
		f.Rotate(true)
	}
}

func (f Field) String() string {
	output := ""
	for index, num := range f.grid {
		if index%3 == 0 && index%9 != 0 {
			output += " |"
		}
		output += " "
		if num == 0 {
			output += " "
		} else {
			output += strconv.FormatInt(int64(num), 10)
		}
		if (index+1)%9 == 0 {
			output = output + "\n"
		}
		if (index+1)%(9*3) == 0 && index+1 != 9*9 {
			output = output + strings.Repeat("-", 9*2+4) + "\n"
		}
	}
	return output
}

func (f Field) IsFull() bool {
	for _, num := range f.grid {
		if num == 0 {
			return false
		}
	}
	return true
}
