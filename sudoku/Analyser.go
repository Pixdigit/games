package sudoku

func ValidMove(f Field, x, y, num int) bool {
	//Horizontal check
	rowIndex := y * 9
	for xSearch := 0; xSearch < 9; xSearch++ {
		if f.grid[rowIndex+xSearch] == num && xSearch != x {
			return false
		}
	}
	//Vertical check
	for ySearch := 0; ySearch < 9; ySearch++ {
		if f.grid[ySearch*9+x] == num && ySearch != y {
			return false
		}
	}

	//BlockCheck
	//use integer division to round down to nearest multiple of 3
	blockIndex := (y/3)*3*9 + (x/3)*3
	for ySearch := 0; ySearch < 3; ySearch++ {
		for xSearch := 0; xSearch < 3; xSearch++ {
			if f.grid[blockIndex+ySearch*9+xSearch] == num && blockIndex+ySearch*9+xSearch != x+y*9 {
				return false
			}
		}
	}
	return true
}

func PossibleValues(f Field, x, y int) []int {
	validNums := make([]int, 0, 9)
	for num := 0; num < 9; num++ {
		if ValidMove(f, x, y, num+1) {
			validNums = append(validNums, num+1)
		}
	}
	return validNums
}

func Hints(f Field) []bool {
	hints := make([]bool, 9*9*9)
	for index, num := range f.grid {
		if num == 0 {
			//num is offset by one
			for num := 0; num < 9; num++ {
				hints[index*9+num] = ValidMove(f, index%9, index/9, num+1)
			}
		}
	}
	return hints
}

func validEmptyFields(f Field) bool {
	hints := Hints(f)
	for index, num := range f.grid {
		if num == 0 {
			cellHints := make([]int, 0, 9)
			for num := 0; num < 9; num++ {
				if hints[index*9+num] {
					cellHints = append(cellHints, num+1)
				}
			}
			if len(cellHints) == 0 {
				return false
			}
		}
	}
	return true
}

func HasErrors(f Field) bool {
	for index, num := range f.grid {
		if !ValidMove(f, index%9, index/9, num) {
			return false
		}
	}
	return !validEmptyFields(f)
}
