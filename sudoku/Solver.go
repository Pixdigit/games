package sudoku

import (
	"errors"
)

func equal(a, b []int) bool {
	if len(a) != len(b) {
		return false
	}

	for i, numA := range a {
		if numA != b[i] {
			return false
		}
	}
	return true
}

func guessOneHint(f Field, guessOffset int) (Field, error) {
	found := false
	smallestCellHintAmount := 2
	for !found {
		for index := range f.grid {
			cellHints := PossibleValues(f, index%9, index/9)
			if f.grid[index] == 0 && len(cellHints) <= smallestCellHintAmount && len(cellHints) != 0 {
				f.grid[index] = cellHints[guessOffset%smallestCellHintAmount]
				found = true
				return f, nil
			}
		}
		if smallestCellHintAmount > 9 {
			return f, errors.New("No available hint")
		}
		smallestCellHintAmount++
	}
	return f, nil
}

func solveTrivialHints(f Field) Field {
	for index := range f.grid {
		cellHints := PossibleValues(f, index%9, index/9)
		if len(cellHints) == 1 {
			f.grid[index] = cellHints[0]
		}
	}
	return f
}

/*
TODO: Solve with guesses
This is the guessing part*/
func solveWithGuesses(f Field, guessOffset int) (solved Field, guesses int, success bool, steps []Field) {
	//One guess per recursion
	guesses++
	if validEmptyFields(f) {
		f, err := guessOneHint(f, guessOffset)
		if err != nil {
			success = false
			return
		}
		var (
			additionalSteps   []Field
			additionalGuesses int
			f2                Field
		)
		f2, additionalGuesses, success, additionalSteps = Solve(f)

		if success {
			guesses += additionalGuesses
			steps = append(steps, additionalSteps...)
			f = f2
		}
		return
	} else {
		success = false
		return
	}
}

func Solve(f Field) (solved Field, guesses int, success bool, steps []Field) {
	oldState := make([]int, 9*9)
	copy(oldState, f.grid)

	for !f.IsFull() && !HasErrors(f) {
		//Copy grid into step array with deref
		stepCopy := make([]int, 9*9)
		copy(stepCopy, f.grid)
		steps = append(steps, LoadField(stepCopy))

		//Fill in fields with only one hint
		f = solveTrivialHints(f)

		//No progress through trivial solves
		if equal(oldState, f.grid) {
			success = false
			var (
				additionalSteps   []Field
				additionalGuesses int
			)
			//Try all possible guesses. May try same solution more than once
			for guessOffset := 0; guessOffset < 9 && !success; guessOffset++ {
				solved, additionalGuesses, success, additionalSteps = solveWithGuesses(f, guessOffset)
			}
			if success {
				guesses += additionalGuesses
				steps = append(steps, additionalSteps...)
			}

			return
		}
		copy(oldState, f.grid)
	}
	success = f.IsFull() && !HasErrors(f)

	stepCopy := make([]int, 9*9)
	copy(stepCopy, f.grid)
	steps = append(steps, LoadField(stepCopy))
	return
}
